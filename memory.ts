/// <reference path="typings/jquery.d.ts" />

class Game
{
    public moves = 0;
    public tilesSelected = 0;

    constructTiles() 
    {
        var signs = "♈♉♊♋♌♍♈♉♊♋♌♍".split('');

        for (var i = 0; i < signs.length; i++) 
        {
          var rand:number = i + Math.floor((signs.length - i) * Math.random());
          var tmp = signs[rand];
          signs[rand] = signs[i];
          signs[i] = tmp;
        }

        var table = $('<table>');
        for (i = 0; i < 3; i++) 
        {
            var tr = $('<tr/>');
            for (var j = 0; j < 4; j++) 
            {
                var td = $('<td/>').append($('<div/>', { text: signs[4*i+j] }));
                tr.append(td);
            }
            table.append(tr)
        }
        $('#gameBorder').append(table);
    }
}

var game = new Game();

$(function()
{ 
  game.constructTiles();
  
  $('td').click(function(){
    if($(this).hasClass('picked') || $(this).hasClass('done'))
    {
      return;
    }

    if (game.tilesSelected == 0)
    {
      $(this).addClass('picked');
      game.tilesSelected++;
    }
    else if (game.tilesSelected == 1)
    {
      if ($('.picked').text()==$(this).text())
      {
        $(this).addClass('done');
        $('.picked').addClass('done').removeClass('picked');
      }
      else
      {
        $(this).addClass('picked');
        $('.picked div').animate({ opacity: 0 }, 
          { queue: false, duration: 1000,
            complete: function() 
            {
              $(this).parent().removeClass('picked');
              $(this).animate({ opacity: 1 }, 0);
            }
        });
      }
      game.tilesSelected = 0;
      game.moves++;
      $('#score').text('Moves: ' + game.moves);
    }
  });
})
